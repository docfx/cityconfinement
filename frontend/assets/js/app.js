import '../css/tailwind.css';
import '../css/app.css';

const $ = require('jquery');

/**
 * Trigger the information toggle
 */
$(function () {
    const headerLinks = $('.header-links-container');
    const burgertoggle = $('#burger-toggle');
    const mainMenu = $('.main-menu');

    $('.info-toggle').on('click', function () {
        const dataId = $(this).data('id');
        $('.service-information-' + dataId).toggle();
    });
    burgertoggle.on('click', function () {
        headerLinks.toggleClass('hidden');
        mainMenu.toggleClass('burger-open');
    });
});

