<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SourceRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Source
{
    use HistoryTrackableEntity;

    public const SOURCE_TYPE_KEY_AUTHORITIES = 0;

    public const SOURCE_TYPE_KEY_OFFICIAL = 10;

    public const SOURCE_TYPE_KEY_FUN = 20;

    public const SOURCE_TYPE_KEY_FAKE = 30;

    public const SOURCE_TYPE_LABEL_AUTHORITIES = 'Authorities';

    public const SOURCE_TYPE_LABEL_OFFICIAL = 'Official';

    public const SOURCE_TYPE_LABEL_FUN = 'Fun';

    public const SOURCE_TYPE_LABEL_FAKE = 'Fake';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, options={"default" = ""})
     */
    private $facebook_url;

    /**
     * @ORM\Column(type="string", length=255, options={"default" = ""})
     */
    private $twitter_url;

    /**
     * @ORM\Column(type="string", length=255, options={"default" = ""})
     */
    private $instagram_url;

    /**
     * @ORM\Column(type="string", length=255, options={"default" = ""})
     */
    private $description;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return (string)$this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl(?string $url): self
    {
        $this->url = (string)$url;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookUrl(): string
    {
        return (string)$this->facebook_url;
    }

    /**
     * @param string|null $facebook_url
     *
     * @return $this
     */
    public function setFacebookUrl(?string $facebook_url): self
    {
        $this->facebook_url = (string)$facebook_url;

        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterUrl(): string
    {
        return (string)$this->twitter_url;
    }

    /**
     * @param string|null $twitter_url
     *
     * @return $this
     */
    public function setTwitterUrl(?string $twitter_url): self
    {
        $this->twitter_url = (string)$twitter_url;

        return $this;
    }

    /**
     * @return string
     */
    public function getInstagramUrl(): string
    {
        return (string)$this->instagram_url;
    }

    /**
     * @param string|null $instagram_url
     *
     * @return $this
     */
    public function setInstagramUrl(?string $instagram_url): self
    {
        $this->instagram_url = (string)$instagram_url;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return (string)$this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = (string)$description;

        return $this;
    }

    /**
     * @return array
     */
    public static function getTypesArray(): array
    {
        return [
            static::SOURCE_TYPE_KEY_AUTHORITIES => static::SOURCE_TYPE_LABEL_AUTHORITIES,
            static::SOURCE_TYPE_KEY_OFFICIAL    => static::SOURCE_TYPE_LABEL_OFFICIAL,
            static::SOURCE_TYPE_KEY_FUN         => static::SOURCE_TYPE_LABEL_FUN,
            static::SOURCE_TYPE_KEY_FAKE        => static::SOURCE_TYPE_LABEL_FAKE,
        ];
    }

    /**
     * @param int $typeKey
     *
     * @return string
     */
    public static function typeKeyToString(int $typeKey): string
    {
        return self::getTypesArray()[$typeKey] ?? '';
    }

    /**
     * @return mixed|string
     */
    public function getTypeAsString()
    {
        return self::getTypesArray()[$this->getType()] ?? '';
    }
}
