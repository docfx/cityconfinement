<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class News
{
    use HistoryTrackableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return (string)$this->text;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText(?string $text): self
    {
        $this->text = (string)$text;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return (string)$this->source;
    }

    /**
     * @param string $source
     *
     * @return $this
     */
    public function setSource(?string $source): self
    {
        $this->source = (string)$source;

        return $this;
    }
}
