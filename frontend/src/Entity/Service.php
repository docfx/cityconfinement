<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Service
{
    use HistoryTrackableEntity;

    public const SERVICE_STATUS_NORMAL = 0;

    public const SERVICE_STATUS_ALTERED = 10;

    public const SERVICE_STATUS_OVERLOADED = 20;

    public const SERVICE_STATUS_PARTIAL_ACTIVITY = 30;

    public const SERVICE_STATUS_SCARCE = 40;

    public const SERVICE_STATUS_STOPPED = 50;

    public const SERVICE_LABEL_NORMAL = 'service.activity.normal';

    public const SERVICE_LABEL_ALTERED = 'service.activity.altered';

    public const SERVICE_LABEL_OVERLOADED = 'service.activity.overloaded';

    public const SERVICE_LABEL_PARTIAL_ACTIVITY = 'service.activity.partial';

    public const SERVICE_LABEL_SCARCE = 'service.activity.scarce';

    public const SERVICE_LABEL_STOPPED = 'service.activity.stopped';

    public const SERVICE_TYPE_PUBLIC_INSTITUTION = 0;

    public const SERVICE_TYPE_STORE = 10;

    public const SERVICE_TYPE_PRIVATE_SERVICE = 20;

    public const SERVICE_LABEL_PUBLIC_INSTITUTION = 'service.type.public.institution';

    public const SERVICE_LABEL_STORE = 'service.type.store';

    public const SERVICE_LABEL_PRIVATE_SERVICE = 'service.type.private.service';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @ORM\Column(type="text")
     */
    private $information;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return (string)$this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = (string)$name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return $this
     */
    public function setSource(?string $source): self
    {
        $this->source = (string)$source;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInformation(): ?string
    {
        return (string)$this->information;
    }

    /**
     * @param string $information
     *
     * @return $this
     */
    public function setInformation(?string $information): self
    {
        $this->information = (string)$information;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return (string)$this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = (string)$phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return (string)$this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = (string)$email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource2(): ?string
    {
        return (string)$this->source2;
    }

    /**
     * @param string $source2
     *
     * @return $this
     */
    public function setSource2(?string $source2): self
    {
        $this->source2 = (string)$source2;

        return $this;
    }

    /**
     * @return array
     */
    public static function getActivityStatus(): array
    {
        return [
            static::SERVICE_STATUS_NORMAL           => static::SERVICE_LABEL_NORMAL,
            static::SERVICE_STATUS_ALTERED          => static::SERVICE_LABEL_ALTERED,
            static::SERVICE_STATUS_OVERLOADED       => static::SERVICE_LABEL_OVERLOADED,
            static::SERVICE_STATUS_PARTIAL_ACTIVITY => static::SERVICE_LABEL_PARTIAL_ACTIVITY,
            static::SERVICE_STATUS_SCARCE           => static::SERVICE_LABEL_SCARCE,
            static::SERVICE_STATUS_STOPPED          => static::SERVICE_LABEL_STOPPED,
        ];
    }

    /**
     * @return array
     */
    public static function getActivityChoices(): array
    {
        return \array_flip(static::getActivityStatus());
    }

    /**
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            static::SERVICE_TYPE_PUBLIC_INSTITUTION => static::SERVICE_LABEL_PUBLIC_INSTITUTION,
            static::SERVICE_TYPE_STORE              => static::SERVICE_LABEL_STORE,
            static::SERVICE_TYPE_PRIVATE_SERVICE    => static::SERVICE_LABEL_PRIVATE_SERVICE,
        ];
    }

    /**
     * @return array
     */
    public static function getTypesChoices(): array
    {
        return \array_flip(static::getTypes());
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return (string)$this->address;
    }

    /**
     * @param string $address
     *
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = (string)$address;

        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getStatusAsString()
    {
        return static::getActivityStatus()[$this->getStatus()] ?? static::SERVICE_LABEL_NORMAL;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed|string
     */
    public function getTypeAsString()
    {
        return static::getTypes()[(int)$this->getType()] ?? static::SERVICE_LABEL_PUBLIC_INSTITUTION;
    }
}
