<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method mixed     findByNameContaining(string $namePart)
 */
class PageRepository extends UniversallySearchableRepository
{
    public const SEARCHABLE_ENTITY_FIELD = 'title';

    public const SEARCHABLE_ENTITY_ORDER_COLUMN = 'title';

    /**
     * PageRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /**
     * @return array|Page[]
     */
    public function findAll(): array
    {
        return $this->findBy([], ['updated' => 'DESC']);
    }
}
