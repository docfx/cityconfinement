<?php

declare(strict_types = 1);

namespace App\Repository;

trait FilterableByTypeEntity
{
    /**
     * Filters an array of Doctrine entity elements,
     * provided the fact that they have a "type" property (of type "int").
     *
     * @param array $entities
     * @param int $type
     *
     * @return array
     */
    public static function filterEntitiesFromArrayByType(array $entities, int $type): array
    {
        foreach ($entities as $key => $entity) {
            if (\is_object($entity) && \method_exists($entity, 'getType') && $entity->getType() !== $type) {
                unset($entities[$key]);
            }
        }

        return $entities;
    }
}
