<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Source;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Source|null find($id, $lockMode = null, $lockVersion = null)
 * @method Source|null findOneBy(array $criteria, array $orderBy = null)
 * @method Source[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method mixed       findByNameContaining(string $namePart)
 */
class SourceRepository extends UniversallySearchableRepository
{
    use FilterableByTypeEntity;

    /**
     * SourceRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Source::class);
    }

    /**
     * @return array|Source[]
     */
    public function findAll(): array
    {
        return $this->findBy([], ['name' => 'DESC']);
    }
}
