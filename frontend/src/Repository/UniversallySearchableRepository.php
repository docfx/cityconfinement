<?php

declare(strict_types = 1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * Class UniversallySearchableRepository.
 *
 * Declared abstract so that Doctrine doesn't try to add it to its unit of work autodetection
 *
 * Entities inheriting this simply have to redeclare the private properties of this one to match their structures.
 * The searchable field HAS to be a string though (any string/text/stringable type).
 */
abstract class UniversallySearchableRepository extends ServiceEntityRepository
{
    public const SEARCHABLE_ENTITY_FIELD = 'name';

    public const SEARCHABLE_ENTITY_ORDER_COLUMN = 'name';

    public const SEARCHABLE_ENTITY_ORDER_DIRECTION = 'ASC';

    public const SEARCHABLE_ENTITIES_LIMIT = null;

    /**
     * Generic method, returns the entities:
     * - matching the given (overloadable) name
     * - sorted by the given (overloadable) column and direction
     * - up to the given (overloadable) limit.
     *
     * @param string $namePart
     *
     * @return mixed
     */
    public function findByNameContaining(string $namePart)
    {
        return $this
            ->createQueryBuilder('e')
            ->select()
            ->where('e.' . static::SEARCHABLE_ENTITY_FIELD . ' LIKE :namePart')
            ->setParameter('namePart', '%' . $namePart . '%')
            ->orderBy('e.' . static::SEARCHABLE_ENTITY_ORDER_COLUMN, static::SEARCHABLE_ENTITY_ORDER_DIRECTION)
            ->setMaxResults(static::SEARCHABLE_ENTITIES_LIMIT)
            ->getQuery()
            ->getResult();
    }
}
