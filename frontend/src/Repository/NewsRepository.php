<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\News;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method mixed     findByNameContaining(string $namePart)
 */
class NewsRepository extends UniversallySearchableRepository
{
    public const SEARCHABLE_ENTITY_FIELD = 'title';

    public const SEARCHABLE_ENTITY_ORDER_COLUMN = 'title';

    /**
     * NewsRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @return array|News[]
     */
    public function findAll(): array
    {
        return $this->findBy([], ['updated' => 'DESC']);
    }

    /**
     * @param int $maxItems
     *
     * @return array|News[]
     */
    public function findLast(int $maxItems): array
    {
        return $this->findBy([], ['updated' => 'DESC'], $maxItems, 0);
    }
}
