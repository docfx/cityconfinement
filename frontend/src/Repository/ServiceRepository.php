<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Service;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method mixed        findByNameContaining(string $namePart)
 */
class ServiceRepository extends UniversallySearchableRepository
{
    use FilterableByTypeEntity;

    /**
     * ServiceRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    /**
     * @return array|Service[]
     */
    public function findAll(): array
    {
        return $this->findBy([], ['name' => 'ASC']);
    }
}
