<?php

declare(strict_types = 1);

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType.
 */
class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add(
                'roles', ChoiceType::class, [
                    'choices'  => [
                        'ROLE_ADMIN'       => 'ROLE_ADMIN',
                        'ROLE_CONTRIBUTOR' => 'ROLE_CONTRIBUTOR',
                        'ROLE_USER'        => 'ROLE_USER',
                    ],
                    'expanded' => true,
                    'multiple' => true,
                ]
            )
            ->add('password');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
