<?php

declare(strict_types = 1);

namespace App\Form;

use App\Entity\Service;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ServiceType.
 */
class ServiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('status', ChoiceType::class, ['choices' => Service::getActivityChoices()])
            ->add('type', ChoiceType::class, ['choices' => Service::getTypesChoices()])
            ->add('name')
            ->add('source')
            ->add('information', CKEditorType::class, ['config_name' => 'cityconfinement_ckeditor', 'required' => false])
            ->add('phone', null, ['required' => false])
            ->add('email', EmailType::class, ['required' => false])
            ->add('source2', null, ['required' => false])
            ->add('address', null, ['required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Service::class,
        ]);
    }
}
