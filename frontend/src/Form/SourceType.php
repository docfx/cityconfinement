<?php

declare(strict_types = 1);

namespace App\Form;

use App\Entity\Source;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SourceType.
 */
class SourceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('type',
                ChoiceType::class,
                [
                    'choices'                   => [
                        Source::SOURCE_TYPE_LABEL_AUTHORITIES => Source::SOURCE_TYPE_KEY_AUTHORITIES,
                        Source::SOURCE_TYPE_LABEL_OFFICIAL    => Source::SOURCE_TYPE_KEY_OFFICIAL,
                        Source::SOURCE_TYPE_LABEL_FUN         => Source::SOURCE_TYPE_KEY_FUN,
                        Source::SOURCE_TYPE_LABEL_FAKE        => Source::SOURCE_TYPE_KEY_FAKE,
                    ],
                    'choice_translation_domain' => true,
                ]
            )
            ->add('description', null, ['required' => false])
            ->add('url', null, ['required' => false])
            ->add('facebook_url', null, ['required' => false])
            ->add('twitter_url', null, ['required' => false])
            ->add('instagram_url', null, ['required' => false]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Source::class,
            ]
        );
    }
}
