<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\News;
use App\Entity\Page;
use App\Entity\Source;
use App\Form\SearchAllEntitiesType;
use App\Model\ServicesSplitter;
use App\Repository\SourceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController.
 */
class MainController extends AbstractController
{
    /**
     * @Route("/",
     *     name="home",
     *     methods={"GET", "HEAD", "OPTIONS", "POST"},
     *     defaults={"_locale" = "fr"}),
     *     requirements={"_locale": "en|fr"}
     * )
     *
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     *
     * @return Response
     */
    public function index(EntityManagerInterface $entityManager, Request $request): Response
    {
        /** @var FormInterface $searchForm */
        $searchForm = $this->createForm(SearchAllEntitiesType::class);
        /** @var array<Source> $officialSources */
        $officialSources = $entityManager->getRepository(Source::class)->findBy(['type' => Source::SOURCE_TYPE_KEY_AUTHORITIES], ['name' => 'ASC']);
        /** @var array<Source> $news */
        $news = $entityManager->getRepository(News::class)->findLast(10);

        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid() && $searchForm->getData() !== null && isset($searchForm->getData()['q'])) {
            return $this->redirectToRoute('search_results', ['q' => $searchForm->getData()['q']]);
        }

        /** @var array $twigVariables */
        $twigVariables = \array_merge(
            [
                'official_sources' => $officialSources,
                'news'             => $news,
                'search_form'      => $searchForm->createView(),
                'q'                => $request->get('q'),
            ],
            (new ServicesSplitter($entityManager))->getSplitServicesByType()
        );

        return $this->render(
            'main/index.html.twig',
            $twigVariables
        );
    }

    /**
     * @Route("/{_locale}/{slug}-{page}",
     *     name="page_public_view",
     *     requirements={"slug" = "[a-zA-Z0-9\-]+", "page" = "\d+", "_locale" = "en|fr"}),
     *     methods={"GET", "HEAD", "OPTIONS"})
     * )
     *
     * @param Page $page
     *
     * @return Response
     */
    public function viewPage(Page $page): Response
    {
        return $this->render(
            'main/page.html.twig',
            [
                'page' => $page,
            ]
        );
    }

    /**
     * @Route("/{_locale}/services",
     *     name="view_services",
     *     requirements={"slug" = "[a-zA-Z0-9\-]+", "page" = "\d+", "_locale" = "en|fr"}),
     *     methods={"GET", "HEAD", "OPTIONS"}
     * )
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function viewServices(EntityManagerInterface $entityManager): Response
    {
        return $this->render(
            'main/view_services.html.twig',
            [
                'services' => (new ServicesSplitter($entityManager))->getSplitServicesByType(),
            ]
        );
    }

    /**
     * @Route("/{_locale}/sources",
     *     name="view_sources",
     *     requirements={"slug" = "[a-zA-Z0-9\-]+", "page" = "\d+", "_locale" = "en|fr"}),
     *     methods={"GET", "HEAD", "OPTIONS"}
     * )
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function viewSources(EntityManagerInterface $entityManager): Response
    {
        /** @var SourceRepository $sourcesRepository */
        $sourcesRepository = $entityManager->getRepository(Source::class);

        /** @var array<Source> $sources */
        $sources = $sourcesRepository->findAll();

        if (\is_array($sources)) {
            /** @var array<Source> $institutionalServices */
            $authoritiesSources = $sourcesRepository::filterEntitiesFromArrayByType($sources, Source::SOURCE_TYPE_KEY_AUTHORITIES);
            /** @var array<Source> $storeServices */
            $officialSources = $sourcesRepository::filterEntitiesFromArrayByType($sources, Source::SOURCE_TYPE_KEY_OFFICIAL);
            /** @var array<Source> $storeServices */
            $funSources = $sourcesRepository::filterEntitiesFromArrayByType($sources, Source::SOURCE_TYPE_KEY_FUN);
            /** @var array<Source> $storeServices */
            $fakeSources = $sourcesRepository::filterEntitiesFromArrayByType($sources, Source::SOURCE_TYPE_KEY_FAKE);
        } else {
            $authoritiesSources = [];
            $officialSources = [];
            $funSources = [];
            $fakeSources = [];
        }

        return $this->render(
            'main/view_sources.html.twig',
            [
                'sources' => [
                    'authorities_sources' => $authoritiesSources,
                    'official_sources'    => $officialSources,
                    'fun_sources'         => $funSources,
                    'fake_sources'        => $fakeSources,
                ],
            ]
        );
    }

    /**
     * @Route("/{_locale}/news",
     *     name="view_news",
     *     requirements={"slug" = "[a-zA-Z0-9\-]+", "page" = "\d+", "_locale" = "en|fr"}),
     *     methods={"GET", "HEAD", "OPTIONS"}
     * )
     *
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function viewNews(EntityManagerInterface $entityManager): Response
    {
        /** @var array<News> $news */
        $news = $entityManager->getRepository(News::class)->findAll();

        return $this->render(
            'main/view_news.html.twig',
            [
                'news' => $news,
            ]
        );
    }
}
