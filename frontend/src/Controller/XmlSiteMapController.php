<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Page;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController.
 */
class XmlSiteMapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml",
     *     name="xml_sitemap",
     *     methods={"GET", "HEAD", "OPTIONS"},
     * )
     *
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     *
     * @throws Exception
     *
     * @return Response
     */
    public function xmlSitemap(EntityManagerInterface $entityManager, Request $request): Response
    {
        $pages = $entityManager->getRepository(Page::class)->findAll();
        $response = $this->render(
            'xml_site_map/xmlsitemap.xml.twig',
            [
                'pages' => $pages,
            ]
        );
        $response->headers->add(['Content-Type' => 'application/xml']);
        $response->setPrivate();
        $response->setDate(new DateTimeImmutable());
        $response->setMaxAge(0);

        return $response;
    }
}
