<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\News;
use App\Entity\Page;
use App\Entity\Service;
use App\Entity\Source;
use App\Form\SearchAllEntitiesType;
use App\Model\SourcesSplitter;
use App\Repository\NewsRepository;
use App\Repository\UniversallySearchableRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController.
 *
 * @Route("/search")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/global/{q}",
     *     name="search_results",
     *     methods={"GET", "HEAD", "OPTIONS", "POST"},
     *     defaults={"_locale" = "fr"}),
     *     requirements={"_locale": "en|fr", "q": ".+"},
     * )
     *
     * @param string $q
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param SourcesSplitter $sourcesSplitter
     *
     * @return Response
     */
    public function index(string $q, EntityManagerInterface $entityManager, Request $request, SourcesSplitter $sourcesSplitter): Response
    {
        /** @var NewsRepository $sourcesRepository */
        $newsRepository = $entityManager->getRepository(News::class);
        /** @var UniversallySearchableRepository $sourcesRepository */
        $servicesRepository = $entityManager->getRepository(Service::class);
        /** @var UniversallySearchableRepository $sourcesRepository */
        $sourcesRepository = $entityManager->getRepository(Source::class);
        /** @var UniversallySearchableRepository $pagesRepository */
        $pagesRepository = $entityManager->getRepository(Page::class);
        /** @var array<News> $sourcesFound */
        $newsFound = $newsRepository->findByNameContaining($q);
        /** @var array<Source> $sourcesFound */
        $sourcesFound = $sourcesRepository->findByNameContaining($q);
        /** @var array<Page> $sourcesFound */
        $pagesFound = $pagesRepository->findByNameContaining($q);
        /** @var array<Service> $sourcesFound */
        $servicesFound = $servicesRepository->findByNameContaining($q);
        /** @var FormInterface $searchForm */
        $searchForm = $this->createForm(SearchAllEntitiesType::class);

        if (\is_array($sourcesFound)) {
            $sourcesFound = $sourcesSplitter->getSplitServicesByTypeFromArray($sourcesFound);
        } else {
            $sourcesFound = [];
        }

        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid() && $searchForm->getData() !== null && isset($searchForm->getData()['q'])) {
            return $this->redirectToRoute('search_results', ['q' => $searchForm->getData()['q']]);
        }

        return $this->render('search/results.html.twig', [
            'results_found' => [
                'news_found'     => $newsFound,
                'services_found' => $servicesFound,
                'sources_found'  => $sourcesFound,
                'pages_found'    => $pagesFound,
            ],
            'search_form'   => $searchForm->createView(),
            'q'             => $q,
        ]);
    }
}
