<?php

declare(strict_types = 1);

namespace App\Model;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;

class ServicesSplitter
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getSplitServicesByType(): array
    {
        /** @var ServiceRepository $servicesRepository */
        $servicesRepository = $this->entityManager->getRepository(Service::class);
        /** @var array<Service> $services */
        $services = $servicesRepository->findAll();

        if (\is_array($services)) {
            /** @var array<Service> $publicServices */
            $publicServices = $servicesRepository::filterEntitiesFromArrayByType($services, Service::SERVICE_TYPE_PUBLIC_INSTITUTION);
            /** @var array<Service> $storeServices */
            $storeServices = $servicesRepository::filterEntitiesFromArrayByType($services, Service::SERVICE_TYPE_STORE);
            /** @var array<Service> $storeServices */
            $privateServices = $servicesRepository::filterEntitiesFromArrayByType($services, Service::SERVICE_TYPE_PRIVATE_SERVICE);
        } else {
            $publicServices = [];
            $storeServices = [];
            $privateServices = [];
        }

        return [
            'institutional_services' => $publicServices,
            'private_services'       => $privateServices,
            'stores_services'        => $storeServices,
        ];
    }
}
