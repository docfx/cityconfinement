<?php

declare(strict_types = 1);

namespace App\Model;

use App\Entity\Source;
use Doctrine\ORM\EntityManagerInterface;

class SourcesSplitter
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Splits an array of sources by type.
     *
     * @param array<Source> $sources
     *
     * @return array
     */
    public function getSplitServicesByTypeFromArray(array $sources): array
    {
        $authoritiesSources = [];
        $officialSources = [];
        $funSources = [];
        $fakeSources = [];

        if (! empty($sources)) {
            foreach ($sources as $source) {
                if ($source instanceof Source) {
                    switch ($source->getType()) {
                        case Source::SOURCE_TYPE_KEY_AUTHORITIES:
                            $authoritiesSources[] = $source;

                            break;
                        case Source::SOURCE_TYPE_KEY_OFFICIAL:
                            $officialSources[] = $source;

                            break;
                        case Source::SOURCE_TYPE_KEY_FUN:
                            $funSources[] = $source;

                            break;
                        case Source::SOURCE_TYPE_KEY_FAKE:
                            $fakeSources[] = $source;

                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return [
            'authorities_sources' => $authoritiesSources,
            'official_sources'    => $officialSources,
            'fun_sources'         => $funSources,
            'fake_sources'        => $fakeSources,
        ];
    }
}
