<?php

declare(strict_types = 1);

namespace App\Twig;

use App\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class FooterExtension.
 */
class FooterExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FooterExtension constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('footer_links', [$this, 'footerLinks']),
        ];
    }

    /**
     * @return array
     */
    public function footerLinks(): array
    {
        return $this->entityManager->getRepository(Page::class)->findBy(['published' => true]);
    }
}
