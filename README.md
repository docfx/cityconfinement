![](readme-sources/cityconfinement.png)

# City Confinement - Information centralization

[![MIT License](https://img.shields.io/badge/Licence-MIT-brightgreen?style=for-the-badge&logo=gitlab)](https://choosealicense.com/licenses/mit/)
[<img src="https://ci.appveyor.com/api/projects/status/9gxx5am2hxop6csw/branch/master?svg=true&retina=true" alt="AppVeyor Build" height="28px"/>](https://ci.appveyor.com/project/docfx/cityconfinement/branch/master)
[![SymfonyInsight](https://insight.symfony.com/projects/c5ba8ed1-8505-46c1-ac01-4af3e33e124c/small.svg)](https://insight.symfony.com/projects/c5ba8ed1-8505-46c1-ac01-4af3e33e124c)

[![Symfony](https://img.shields.io/badge/BuiltWith-Symfony-blueviolet)](https://www.symfony.com)
[![tailwindcss](https://img.shields.io/badge/BuiltWith-tailwindcss-blueviolet)](https://tailwindcss.com)
[![webpack](https://img.shields.io/badge/BuiltWith-webpack-blueviolet)](https://webpack.js.org)
[![Doctrine](https://img.shields.io/badge/BuiltWith-Doctrine-blueviolet)](https://www.doctrine-project.org)


```
*Warning:* this project [DevOps] is only working with Windows 10 (x64) so far.
It lays the basics to set up three working environments: `dev`, `qa` and `prod`.
```
Open project to handle the basic information for a city, to help citizens organize and centralize information during confinement.

This was opened during the SARS-CoV2 crisis in Europe.

The project currently supports the following locales (ISO):
- fr
- en

All translations are welcome! :)


## Setup

The setup is lightning fast, you just have to start the [integrated Symfony server](https://symfony.com/doc/current/setup/symfony_server.html).

Via :

- `symfony server:start -d`

In your IDE (if you use code structure following, like in PHPStorm, for example), 
mark the following directories as *excluded*:
- `/devops-sources`
- `/node_modules`
- `/var`
- `/vendor`

And the following directories as *test sources*:
- `/tests`

## Production deployment

**PRODUCTION ONLY**

*You can copy the file `production-deplyment.sh.dist` found at the root of this project and customize it.*

You'll need to create the file at: `environment-files/.env.prod.local`, 
by copying `environment-files/.env.[environment].local-dist` located at the same place.

Then, you'll need to edit all the `MYSQL_*` variables with your local credentials and options.

Then, you'll need to either set up the production environment by adding an shell Env parameter for your app.
Or, in the worst case, create also a file at: `environment-files/.env.local`, containing only this:

```ini
# Application environment - this is for [prod]
APP_ENV=prod
``` 

Last but not the least, you'll need to create the database once everything is set up by using:
`php frontend/bin/console doctrine:database:create --env=prod`

And then create the schema for the first time by using:
`php frontend/bin/console doctrine:schema:create --env=prod`

You should be all set up!

You might want to run `npm run encoreprod` from the `frontend` directory if you changed the assets.

Be careful, though: the project is set up to ONLY answer on `HTTPS` (have a look at the config YAML files).

## Configuration

You'll probably need to edit a few things:
- The `frontend/assets/images/ogimage.jpg` that serves as OpenGraph preview for Facebook.
- The `environment-files/.env` default variables (your website title, subtitle, and description, for frontend, OG, and `<meta>`)
- The metadata contained (globally if not overloaded) in `frontend/templates/base.html.twig` `<head>` markup.
- The custom links provided only as fixed, static HTML in the header and in the main right block on the home page.

## Containers - WIP

The Docker containers have not been set up so far.

If you want to build them or work on them, just, under Windows 10, in a shell, call:

```
init-project.bat
build-all-docker-containers.bat
```

Then modify your hosts file (usually `C:\Windows\System32\drivers\etc\hosts` or `/etc/hosts`) as follows:

```
    # Local URLs -- routed to respective environments
    # "prod" environment on local devices
    127.0.0.1 prod.cityconfinement.cityconfinement.local
    127.0.0.1 cdn.prod.cityconfinement.cityconfinement.local
    # "qa" environment on local devices
    127.0.0.1 qa.cityconfinement.cityconfinement.local
    127.0.0.1 cdn.qa.cityconfinement.cityconfinement.local
    # "dev" environment on local devices
    127.0.0.1 dev.cityconfinement.cityconfinement.local
    127.0.0.1 cdn.dev.cityconfinement.cityconfinement.local
```

And then you just have to type in your browser (after doing all the replacements above):

`http(s)://[env].cityconfinement.cityconfinement.local:101[env-number]1`

With \[env-number] being (look at the appropriate docker-compose files):

* prod: 0
* qa:   1
* dev:  2

*Note:*
This project explicits all containers virtual internal IP addresses and TCP ports instead of using Docker auto networking.
This requires users to do this for all projects, but has the advantage of offering 100% copy/paste URL + ports between
any person working in the project (especially during Docker setup).

## 🌲 The directory tree (as expected and generated by `init-project.bat`)

```
    + [Your project name, root directory]
    |
    |---+ /devops-sources
    |   |
    |   +--- Here you can put everywhere linked to continuous quality and integration (analysis, formatting, auto-fixing, ...)
    |   +--- ...
    |
    |---+ /docker-sources -- your docker sources
    |   |
    |   +---+ /containers-config
    |   |   |
    |   |   +---+ /[technology]
    |   |   |   |
    |   |   |   +---+ /[dev]-[technology]
    |   |   |   |   |
    |   |   |   |   +--- dev-[technology]-container-config.docker
    |   |   |   |   +--- ... plus any other necessary files to set up development
    |   |   |   |
    |   |   |   +---+ /[qa]-[technology]
    |   |   |   |   |
    |   |   |   |   +--- qa-[technology]-container-config.docker
    |   |   |   |   +--- ... plus any other necessary files to set up qa
    |   |   |   |
    |   |   |   +---+ /[prod]-[technology]
    |   |   |   |   |
    |   |   |   |   +--- prod-[technology]-container-config.docker
    |   |   |   |   +--- ... plus any other necessary files to set up prod
    |   |   |
    |   |   +---+ /[another technology]
    |   |       |
    |   |       +--- ...
    |   |
    |   +--- dev-docker-compose.yaml -- your docker-compose sources for each environment
    |   +--- global-docker-compose.yaml
    |   +--- prod-docker-compose.yaml
    |   +--- qa-docker-compose.yaml
    |
    |---+ /environment-files -- all your local environment files, shared between Docker and the containers technologies
    |   |
    |   +--- .env -- the global key/values for all your containers on all environments
    |   +--- .env.[environment].local-dist -- your local template to create local .env files
    |   +--- .env.dev -- one for each environment
    |   +--- .env.qa
    |   +--- .env.prod
    |
    |---+ /git-hooks-sources -- this directory contains the sources of what you should put into your git `.git/hooks` directory
    |   |
    |   +--- commit-msg
    |   +--- commit-msg.bat
    |   +--- pre-commit
    |   +--- pre-commit.bat
    |
    |---+ /ci-jobs
    |   |
    |   +--- Here you can put any backup of your CI integration jobs and tasks (better save them than sorry ^^)
    |
    |---+ /readme-sources
    |   |
    |   +--- Here you put whatever files `README.md` uses
    |
    +--- .dockerignore
    +--- .gitignore
    |
    +--- init-project.bat -- initalizes or reapplies project basic configuration
    +--- build-all-docker-containers.bat
    +--- build-dev-docker-containers.bat
    +--- build-qa-docker-containers.bat
    +--- build-prod-docker-containers.bat
    |
    +--- README.md
```

## 🐳 Docker

Everything lies in the `docker-sources` directory. You'll find explained and detailed
Dockerfiles. Don't forget to keep their verbosity.

To set up all containers and start the app, just use:

     build-all-docker-containers.bat

Or any other one if you just want to rebuild and start one environment at a time.

## Contributing

The project is open for PRs. 
Please feel free to open discussions/bugs/PRs.
