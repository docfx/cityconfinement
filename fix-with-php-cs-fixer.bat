:: ################################################################################
:: # CityConfinement - Windows - PHP-CS-Fixer fixing                               #
:: ################################################################################

:: Shut down Windows current command echoing
@ECHO OFF

:: Change the title of the window, for pimpin' reasons :)
:: JK: this helps finding it among all open windows
TITLE CityConfinement - PHP-CS-Fixer --fix on the /frontend directory

:: Ask for confirmation first, for this takes a long time and can be called by mistake
ECHO -------------------------------------------------------------------------------------------------
ECHO - CityConfinement code fixing                                                                   -
ECHO - (you need to have PHP-CS-Fixer in /devops-sources/php-cs-fixer-v2.phar installed to proceed)  -
ECHO -------------------------------------------------------------------------------------------------

:: If command line argument is "-y", then just go on, don't ask.
IF /I "%~1"=="-y" (GOTO :start)

:: If command line argument is --help, then just go on, don't ask.
IF /I "%~1"=="--help" (GOTO :explain) ELSE (GOTO :ask)
:explain
ECHO.
ECHO Usage: fix-with-php-cs-fixer.bat [OPTION]
ECHO Fix all PHP source code, adoping the rules defined as in /devops-sources/.php_cs.dist
ECHO.
ECHO -y                 force without confirmation
ECHO --help             display this help screen
GOTO :end

:: Ask people if they really want to proceed
:ask
ECHO.
SET /P confirm="Are you SURE you want to automatically fix the PHP source code? (Y/y/N/n) "
ECHO.

:: Use that variable prompted from user to go further or not
:: Note that Windows shell scripts don't really like multiple commands and nesting
:: So we just use ugly loops for now. :(
IF /I "%confirm%"=="y" (GOTO :start) ELSE (GOTO :bypass)

:: Start the production containers
:start

:: Start fixing!
ECHO.
ECHO --------------------------------------------------------------
ECHO Applying PHP-CS-Fixer custom rules to the /frontend directory
ECHO --------------------------------------------------------------
"./devops-sources/php-windows-binaries/php.exe" "./devops-sources/php-cs-fixer-v2.phar" fix --show-progress dots --verbose --config=./devops-sources/.php_cs.dist
ECHO ---------------------------------------------------

:: Confirm what has been done
ECHO.
ECHO -----------------------------------------------------
ECHO PHP-CS-Fixer fixing execution complete
ECHO -----------------------------------------------------

:: Just stop here and go to the end!
GOTO end

:: If the user answered "n"
:bypass
ECHO OK, then. See you around! :)
ECHO ---------------------------------------------------

:: The end!
:end
